//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class AudioDb
        : DatabaseTable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public AudioClip[] chatter                              { get; private set; }
        public AudioClip[] clips                                { get; private set; }
        public AudioClip[] music                                { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
            chatter = LoadAll<AudioClip>("audio/chatter"); 
            clips = LoadAll<AudioClip>("audio/clips");
            music = LoadAll<AudioClip>("audio/music");
        }

        //
        // --------------------------------------------------------------------
        //
        
        public AudioClip FindChatter(string name)
        {
            return FindClip(chatter, name);
        }

        //
        // --------------------------------------------------------------------
        //

        public AudioClip FindClip(string name)
        {
            return FindClip(clips, name);
        }

        //
        // --------------------------------------------------------------------
        //

        public AudioClip FindMusic(string name)
        {
            return FindClip(music, name);
        }        

        //
        // private methods ////////////////////////////////////////////////////
        //

        private AudioClip FindClip(AudioClip[] arr, string name)
        {
            return System.Array.Find(arr, (clip)=>clip.name == name);
        }
    }
}
