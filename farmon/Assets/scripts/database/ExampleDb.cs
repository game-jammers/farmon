//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEditor;

namespace Farmon
{
    public class ExampleDb
        : DatabaseTable
    {
        //
        // members/////////////////////////////////////////////////////////////////
        //
        
        [ShowInInspector] public GameManager[] managers         { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////////
        //
    
        public override void Initialize(Database db)
        {
           managers = LoadAll<GameManager>("managers"); 
        }
    }
}
