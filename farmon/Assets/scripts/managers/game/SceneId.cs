//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public enum SceneId
    {
        MainMenu   = 0,
        IntroScene = 1,
        GameScene = 2,
    }
}
