//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Farmon
{
    public static class Formatter
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        private static readonly Regex kRx = new Regex(@"\$(\w+?)\$", RegexOptions.Compiled);

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static string Format(string input, Dictionary<string, string> data)
        {
            return kRx.Replace(input, (match)=>{
                string key = match.Groups[1].Value;
                string res = key;
                data.TryGetValue(key.ToUpper(), out res);
                UnityEngine.Debug.Log(System.String.Format("Replacing {0} with {1}", key, res));
                return res;
            });
        }
    };
}
