//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Farmon
{
    public class GameManager
        : BaseGameManager
    {
        //
        // constants //////////////////////////////////////////////////////////
        //
        
        public static readonly string kPrefabPath               = "managers/GameManager";

        //
        // accessors ////////////////////////////////////////////////////////////
        //

        public static new GameManager instance                  { get; private set; }

        //
        // members ////////////////////////////////////////////////////////////
        //

        private Dictionary<string, string> gameData              = new Dictionary<string, string>();
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static GameManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
            }

            return instance;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public string Format(string input)
        {
            return Formatter.Format(input, gameData);
        }

        //
        // --------------------------------------------------------------------
        //

        public void SetVar(string key, string value)
        {
            gameData[key.ToUpper()] = value;
        }
        

        //
        // --------------------------------------------------------------------
        //

        public string GetVar(string key)
        {
            string result = System.String.Empty;
            gameData.TryGetValue(key.ToUpper(), out result);
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ChangeScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Single );
        }

        //
        // --------------------------------------------------------------------
        //

        public static void AddScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Additive );
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings()
        {
            ApplySettings(settings);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ApplySettings(GameSettings settings)
        {
            this.settings = settings;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            ApplySettings();
        }
    };
}
