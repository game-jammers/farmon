//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Farmon
{
    public class IntroSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Intro Cutscene")]
        public Inkwell.InkScenePlayer introCutscene;
        public LoopScroll parallax;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(IntroCutscene());
        }

        //
        // cutscenes //////////////////////////////////////////////////////////
        //

        private IEnumerator IntroCutscene()
        {
            introCutscene.fader.Show(0f, null);
            introCutscene.Load();

            yield return introCutscene.PlayAsync(OnInkEvent);
            Dbg.Log("Cutscene Complete");
            GameManager.ChangeScene(SceneId.GameScene);
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator OnInkEvent(Inkwell.InkCommand cmd)
        {
            var wait = new WaitForSeconds(1f);
            if(cmd is Inkwell.InkUserEvent ev)
            {
                if(ev.name == "StartParallax")
                {
                    parallax.active = true;
                }

                yield return wait;
            }
        }
        
        
    }
}
