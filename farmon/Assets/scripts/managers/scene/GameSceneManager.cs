//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using Cinemachine;
using System.Collections;
using UnityEngine;

namespace Farmon
{
    public class GameSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Train Station Cutscene")]
        public bool paused                                      = false;
        public Inkwell.InkScenePlayer trainStationCutscene;

        [Header("Characters")]
        public GameObject playerDoll;
        public GameObject mayorDoll;
        public Character playerPrefab;
        public Character petPrefab;
        public Character mayorPrefab;

        [Header("Camera")]
        public CinemachineVirtualCamera playerCam               = null;

        //
        private Character player;
        private Character pet;
        private Character mayor;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            if(GameManager.instance.GetVar("CUT_TrainStation") != "complete")
            {
                StartCoroutine(TrainStationCutscene());
            }
            else
            {
                paused = false;
            }
        }

        //
        // cutscenes //////////////////////////////////////////////////////////
        //

        private IEnumerator TrainStationCutscene()
        {
            paused = true;
            trainStationCutscene.preprocessStr = GameManager.instance.Format;
            trainStationCutscene.fader.Show(0f, null);
            trainStationCutscene.Load();
            yield return trainStationCutscene.PlayAsync(OnInkEvent);
            GameManager.instance.SetVar("CUT_TrainStation", "complete");

            player = Instantiate(playerPrefab, playerDoll.transform.position, playerDoll.transform.rotation);
            pet = Instantiate(petPrefab, playerDoll.transform.position + Vector3.forward, Quaternion.identity);
            mayor = Instantiate(mayorPrefab, mayorDoll.transform.position, mayorDoll.transform.rotation);
            Destroy(playerDoll.gameObject);
            Destroy(mayorDoll.gameObject);

            FollowDriver petFollow = pet.GetComponent<FollowDriver>();
            petFollow.leader = player.transform;

            player.Initialize();
            pet.Initialize();
            mayor.Initialize();

            playerCam.Priority = 10;
            playerCam.LookAt = player.transform;
            playerCam.Follow = player.transform;

            paused = false;
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator OnInkEvent(Inkwell.InkCommand cmd)
        {
            if(cmd is Inkwell.InkUserEvent uev && uev.name == "SetName")
            {
                UIInputDialog inputDlg = trainStationCutscene.dialogs[Inkwell.InkDialog.DialogType.Input] as UIInputDialog;
                string name = inputDlg.inputText;
                GameManager.instance.SetVar("playername", name);
                yield break;
            }
        }
    }
}
