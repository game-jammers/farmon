//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class SceneManager
        : BaseSceneManager
    {
        // events //////////////////////////////////////////////////////////////
        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        // members /////////////////////////////////////////////////////////////
        public static new SceneManager instance                 { get; private set; }
        public DialogManager dialog;
        public GameObjectPools pools                            = new GameObjectPools();

        // unity callbacks /////////////////////////////////////////////////////
        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();
        }

        // protected methods ///////////////////////////////////////////////////
        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}
