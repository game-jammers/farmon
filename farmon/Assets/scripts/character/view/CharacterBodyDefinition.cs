//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Farmon
{
    [CreateAssetMenu(fileName="CharacterBodyDefinition", menuName="blacktriangles/CharacterBody Definition")]
    public class CharacterBodyDefinition
        : ScriptableObject
    {
        public CharacterBodyParts parts;
    }
}
