//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class CharacterView
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////////
        //
        
        public CharacterBody body;
        public Animator animator                                { get { return body == null ? null : body.animator; } }


        private float speed                                     = 0f;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            if(body != null)
                body.character = _character;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Update()
        {
            if(!initialized) return;

            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            float curvel = 0f;
            if(state.isMoving)
            {
                character.transform.rotation = Quaternion.Lerp(character.transform.rotation, Quaternion.LookRotation(state.moveDir, Vector3.up), 0.1f);
                speed = Mathf.SmoothDamp(speed, 1f, ref curvel, 0.025f);
            }
            else
            {
                speed = Mathf.SmoothDamp(speed, 0f, ref curvel, 0.025f);
            }

            animator.SetFloat("speed", speed);
        }
    }
}
