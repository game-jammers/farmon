//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace Farmon
{

    public abstract class ModularCharacterView
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public CharacterBodyDefinition bodyDefinition           = null;
        public CharacterBody body                               = null;
        public Animator animator                                { get { return body == null ? null : body.animator; } }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Rebuild(IEnumerable<CharacterBodyPart> parts)
        {
            List<SkinnedMeshRenderer> renderers = new List<SkinnedMeshRenderer>();
            List<CharacterBodyPart> usedParts = new List<CharacterBodyPart>();
            List<CharacterBodyPart> attachments = new List<CharacterBodyPart>();

            CharacterBody rig = null;
            foreach(CharacterBodyPart part in parts)
            {
                if(part == null) continue;
                if(part.type == CharacterBodyPart.Type.Attachment)
                {
                    attachments.Add(part);
                }
                else
                {
                    if(rig == null)
                    {
                        rig = part.rig;
                    }

                    Dbg.Assert(rig == part.rig, "All rigs should match!");
                    usedParts.Add(part);
                    renderers.AddRange(part.transform.GetComponentsInChildren<SkinnedMeshRenderer>());
                }
            }

            MeshUtility.MergeResult merged = Rebuild(rig, renderers, attachments);

            // add attachments
            foreach(CharacterBodyPart part in attachments)
            {
                CharacterBodyPart instance = Instantiate(part) as CharacterBodyPart;
                instance.transform.SetParent(body.hardpoints[part.attachPoint], false);
                instance.transform.localPosition = Vector3.zero;
                instance.transform.localRotation = Quaternion.identity;
                instance.ApplyColors();
            }

            // update materials on the merged mesh
            int matIdx = 0;
            foreach(CharacterBodyPart part in usedParts)
            {
                List<Material> sharedMats = part.GetSharedMaterials();
                foreach(Material mat in sharedMats)
                {
                    part.ApplyColors(merged.skin.materials[matIdx++]);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public MeshUtility.MergeResult Rebuild(CharacterBody rig, IEnumerable<SkinnedMeshRenderer> renderers, IEnumerable<CharacterBodyPart> attachments)
        {
            DestroyBody();

            MeshUtility.MergeResult res = MeshUtility.MergeSkinnedMeshes(rig.gameObject, renderers);
            body = res.root.GetComponent<CharacterBody>();
            body.transform.SetParent(transform, worldPositionStays: false);

            return res;
        }

        //
        // ------------------------------------------------------------------------
        //
        
        public void DestroyBody()
        {
            if(body != null)
            {
                GameObject.Destroy(body.gameObject);
                body = null;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Awake()
        {
            base.Awake();
            if(bodyDefinition != null)
            {
                Rebuild(bodyDefinition.parts);
            }
        }
    }
}
 
