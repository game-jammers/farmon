//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class CharacterBody
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public Character character;
        public Animator animator;

        //
        // --------------------------------------------------------------------
        //

        public enum Hardpoint
        {
            HandL,
            HandR,
            Head,
            Back,
            Chest,
            ThighL,
            ThighR,
            CalfL,
            CalfR
        };

        //
        // ------------------------------------------------------------------------
        //

        [System.Serializable]
        public class Hardpoints
            : EnumList<Hardpoint, Transform>
        {
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Hardpoints hardpoints;
    }
}
