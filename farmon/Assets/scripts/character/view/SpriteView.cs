//
// (c) BLACKTRIANGLES 2021
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class SpriteView
        : CharacterView
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private CardinalDirection facing = CardinalDirection.South;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Update()
        {
            if(!initialized) return;
            
            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            if(state.isMoving)
            {
                // horz
                if(btMath.Abs(state.moveDir.x) > btMath.Abs(state.moveDir.z))
                {
                    if(state.moveDir.x > 0f)
                    {
                        facing = CardinalDirection.East;
                    }
                    else
                    {
                        facing = CardinalDirection.West;
                    }
                }
                else
                {
                    if(state.moveDir.z < 0f)
                    {
                        facing = CardinalDirection.North;
                    }
                    else
                    { 
                        facing = CardinalDirection.South;
                    }
                }

                switch(facing)
                {
                    case CardinalDirection.North: animator.Play("Walk_N"); break;
                    case CardinalDirection.South: animator.Play("Walk_S"); break;
                    case CardinalDirection.East: animator.Play("Walk_E"); break;
                    case CardinalDirection.West: animator.Play("Walk_W"); break;
                }
            }
            else
            {
                switch(facing)
                {
                    case CardinalDirection.North: animator.Play("Idle_N"); break;
                    case CardinalDirection.South: animator.Play("Idle_S"); break;
                    case CardinalDirection.East: animator.Play("Idle_E"); break;
                    case CardinalDirection.West: animator.Play("Idle_W"); break;
                }
            }
        }
        
    }
}
