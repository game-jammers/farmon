//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{

    public class CharacterControl
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UnityEngine.CharacterController unityController  = null;
        
        //
        // init ///////////////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);

            if(unityController == null)
            {
                unityController = GetComponent<UnityEngine.CharacterController>();
            }
        }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Move(Vector3 movedir)
        {
            datamodel.state.moveDir = movedir;
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Update()
        {
            if(!initialized) return;

            CharacterState state = datamodel.state;
            CharacterStats stats = datamodel.stats;

            Vector3 deltaMove = datamodel.state.moveDir * stats.moveSpeed;
            deltaMove += Physics.gravity;
            unityController.Move(deltaMove * Time.deltaTime);
            state.isGrounded.Set(unityController.isGrounded);
        }
    }

}
