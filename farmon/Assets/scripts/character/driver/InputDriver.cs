//
// (c) {{company}} 2020
// {{url}}
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class InputDriver
        : CharacterDriver
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public enum Action
        {
            MoveX,
            MoveY,
            LookX,
            LookY,
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public btCamera cam                                     { get; private set; }
        public bool hasCamera                                   { get { return cam != null; } }

        private InputRouter<Action> input;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Initialize(Character character)
        {
            base.Initialize(character);
            
            GameSettings.Mouse mouseSettings = GameManager.instance.settings.mouse;
            input = new InputRouter<Action>();
            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.LeftArrow, KeyCode.RightArrow));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.DownArrow, KeyCode.UpArrow));

            SceneManager.instance.RequestCamera(character);
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Update()
        {
            if(!initialized) return;

            //
            // MOVE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //

            Vector3 move = new Vector3(
                input.GetAxis(Action.MoveX),
                0f,
                input.GetAxis(Action.MoveY));

            move = cam.TransformDirection(move);
            move.y = 0f;
            controller.Move(move);
        }
        
        //
        // ICameraController //////////////////////////////////////////////////
        //

        public void OnTakeCamera(btCamera camera)
        {
            cam = camera;
        }

        //
        // --------------------------------------------------------------------
        //

        public void OnReleaseCamera()
        {
            cam = null;
        }
        
        
        
    }
}
