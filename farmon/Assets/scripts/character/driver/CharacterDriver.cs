//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{

    public class CharacterDriver
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
    
        public CharacterControl controller                          { get; private set; }
    
        //
        // ////////////////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            controller = character.controller;
        }
    }

}
