//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class FollowDriver
        : CharacterDriver
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Transform leader;
        public float followDistance                             = 2f;

        private bool isFollowing                                = false;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        public virtual void Update()
        {
            if(!initialized) return;
            if(leader == null) return;

            //
            // MOVE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            //

            Vector3 dir = leader.position - character.transform.position;
            float dist2 = dir.sqrMagnitude;
            float follow2 = followDistance * followDistance;
            if(dist2 > follow2 * 2f)
            {
                isFollowing = true;
            }
            else if(dir.sqrMagnitude < follow2)
            {
                isFollowing = false;
            }

            if(isFollowing)
            {
                dir.y = 0;
                controller.Move(dir.normalized);
            }
            else
            {
                controller.Move(Vector3.zero);
            }
        }

        
    }
}
