//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{

    public class CharacterDatamodel
        : CharacterComponent
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public CharacterState state                             { get; private set; }
        public CharacterStats stats                             { get { return _stats; } }
        [SerializeField] private CharacterStats _stats          = CharacterStats.Default;

        //
        // initialize /////////////////////////////////////////////////////////
        //
        
        public override void Initialize(Character _character)
        {
            base.Initialize(_character);
            state = new CharacterState(_character, stats);
        }
        
    }

}
