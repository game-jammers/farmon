//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    [System.Serializable]
    public class CharacterState
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class TimestampedBool
            : TimestampedValue<bool>
        {
            public TimestampedBool(bool start)
                : base(start)
            {
            }
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //



        public Vector3 moveDir                                  = Vector3.zero;
        public bool isMoving                                    { get { return moveDir.sqrMagnitude > Mathf.Epsilon; } }
        public TimestampedBool isGrounded                       = new TimestampedBool(false);

        //
        // constructor ////////////////////////////////////////////////////////
        //

        public CharacterState(Character character, CharacterStats stats)
        {
        }
    }
}
