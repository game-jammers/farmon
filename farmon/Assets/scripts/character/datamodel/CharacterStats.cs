//
// (c) LetsMakeGames 2019
// http://letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    [System.Serializable]
    public struct CharacterStats
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("MOVEMENT")]
        public float moveSpeed;

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public static CharacterStats Default 
        { 
            get 
            {
                return new CharacterStats() {
                    moveSpeed = 3f
                };
            }
        }

    }
}
