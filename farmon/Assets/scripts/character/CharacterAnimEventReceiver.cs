//
// (c) LetsMakeGames 2021
// http://www.letsmake.games
//

using blacktriangles;
using UnityEngine;

namespace Farmon
{
    public class CharacterAnimEventReceiver
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public CharacterBody body;

        public AudioVariation defaultSound;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void FootL()
        {
            PlayFootstep();
        }

        //
        // --------------------------------------------------------------------
        //

        public void FootR()
        {
            PlayFootstep();
        }

        //
        // --------------------------------------------------------------------
        //

        public void PlayFootstep()
        {
            //CharacterState state = body.character.datamodel.state;
            //if(state.isGrounded)
            //{
            //    AudioVariation sound = defaultSound;
            //    RaycastHit hit = default(RaycastHit);
            //    if(Physics.Raycast(transform.position, Vector3.down, out hit, 0.1f))
            //    {
            //        FootstepMaterial mat = hit.collider.GetComponent<FootstepMaterial>();
            //        if(mat != null)
            //        {
            //            sound = mat.footstepSound;
            //        }
            //    }

            //    if(sound != null)
            //    {
            //        Debug.Log("PLAY");
            //        sound.Play();
            //    }
            //}
        }
    }
}
